import React from 'react';
import { connect } from 'react-redux'

const HIHI = () => (
  <h1>hahaha</h1>
);
class App extends React.Component {
  state = {
    isAdding: true
  }
  toggle = () => {
    // this.setState({
    //   isAdding: !this.state.isAdding
    // })
    var { dispatch } = this.props;
    dispatch({ type: 'toggle' });
  }
  addItem = () => {
    var { dispatch } = this.props;
    dispatch({ type: "add-item", item: this.refs.txtThem.value })
  }
  xoaItem = (index) => {
    var { dispatch } = this.props;
    dispatch({ type: "remove", index })
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
        </header>
        <body>
          <ul>
            {this.props.mang.map((e, i) => {
              return <li>STT:{i} - {e}
                <button type="button" onClick={() => this.xoaItem(i)}>Xoa</button>
              </li>
            })}
          </ul>
          <HIHI></HIHI>
          {
            this.props.isAdding ? <div>
              <input type="text" ref="txtThem" />
              <button type="button" onClick={() => this.toggle()}>Đóng</button>
              <button type="button" onClick={() => this.addItem()}>save</button>
            </div>
              : <button onClick={() => this.toggle()}>Thêm mới</button>
          }


        </body>
      </div>
    );
  }
}
export default connect(function (state) {
  return {
    mang: state.mang,
    isAdding: state.isAdding
  }
})(App);
