var mangReduce = (state = ['viet', 'nam', 'bac'], action) => {
    switch (action.type) {
        case 'add-item':
            return [...state, action.item]
        case 'remove':
            return state.filter((e, idex) => idex !== action.index)
        default:
            return state
    }
}
// export default mangReduce;
export default mangReduce