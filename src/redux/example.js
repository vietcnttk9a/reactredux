import { createStore, combineReducers, compose } from 'redux'
import reduce from './reduce/reduce'

var store = createStore(reduce, compose(
    window.devToolsExtension ? window.devToolsExtension() : f => f
));
store.subscribe((e) => {
    // var str = store.getState();
    // document.getElementById('detail').innerHTML = "<pre>" + JSON.stringify(str, null, 2) + "</pre>"
})

store.dispatch({ type: 'toggle' })

store.dispatch({ type: 'remove', index: 1 });

export default store;